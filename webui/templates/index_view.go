package templates

import (
	"html/template"
	"io"

	"github.com/labstack/echo/v4"
)

var _ echo.Renderer = (*IndexView)(nil)

type IndexView struct {
	templ *template.Template
}

func NewIndexView(templ *template.Template) *IndexView {
	return &IndexView{templ: templ}
}

func (t *IndexView) Render(w io.Writer, name string, data any, c echo.Context) error {
	if err := t.templ.ExecuteTemplate(w, name, data); err != nil {
		return err
	}
	return nil
}
