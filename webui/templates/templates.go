package templates

import (
	"embed"
	"html/template"
)

var (
	//go:embed "templates/*"
	whisperTemplates embed.FS
)

func NewTemplates() (*template.Template, error) {
	return template.ParseFS(whisperTemplates, "templates/*.gohtml")
}
