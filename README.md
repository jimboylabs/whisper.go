# Whisper.go

Proof of concept of using [Temporal.io](https://temporal.io/) for running [whisper.cpp](https://github.com/ggerganov/whisper.cpp) to transcribe an audio into text.

## Goals

1. User able to upload mp3 file and receive the result.
2. Should be able to upload and process large files, usually podcast file range to 50-100MB depend on the duration.

## Installation

### Before you start

You will need to have the following installed or configured, before proceeding:

- [Docker](https://www.docker.com/get-started/) and [Docker compose](https://docs.docker.com/compose/gettingstarted/)
- [Tilt.dev](https://docs.tilt.dev/)
- [Temporal CLI](https://docs.temporal.io/cli#installation)
- [Go](https://go.dev/dl/)
- [Whisper.cpp](https://github.com/ggerganov/whisper.cpp)
- [ffmpeg](https://ffmpeg.org/)

### Installing ffmpeg

ffmpeg is required for convert mp3 files into WAV, currently whisper.cpp only support 16-bit WAV files.

```
sudo apt-get install ffmpeg
```

### Installing Whisper.cpp

1. Create a directory for storing the project `mkdir -p ~/clones`
1. Clone the project to home directory `git clone git@github.com:ggerganov/whisper.cpp.git ~/clones/whisper.cpp`
1. Build the main example `cd whisper.cpp && make`

### Running the project

1. Clone the project to home directory `git clone git@gitlab.com:jimboylabs/whisper.go.git ~/clones/whisper.go`
1. Run `tilt up`
1. Now you should be able to run the temporal server, whisper.go web server and worker
1. Open `http://localhost:3000`
1. Open Temporal dashboard `http://localhost:8233`

## Architecture

### Sequence Diagram

```mermaid
sequenceDiagram
    User->>Whisper.go server: Upload audio (wav/mp3) file
    Whisper.go server->>Temporal: Execute workflow
    Temporal->>Whisper.go worker: Schedule transcribe workflow
    Whisper.go server->>Temporal: Check workflow status periodically
    Temporal->>Whisper.go server: Return workflow status
    Whisper.go server->>User: Return output
```

## Demo

[https://youtu.be/PMbrBnY9bqw](https://youtu.be/PMbrBnY9bqw)

### Screenshots

<details>
<summary>Tilt dashboard</summary>

![Tilt](screenshots/screenshot-running-worker.png)

</details>

<details>
<summary>Temporal dashboard</summary>
![Temporal](screenshots/screenshot-temporal-webui.png)
![Temporal Activity](screenshots/screenshot-temporal-activity.png)
</details>

<details>
<summary>Whisper.go web ui</summary>
![Result](screenshots/screenshot-result.png)
</details>
