package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type EchoFramework struct {
	*echo.Echo
	*App
}

func SetupEchoRouter(wf *echo.Echo, app *App) {
	r := &EchoFramework{Echo: wf, App: app}

	r.Use(middleware.Logger())

	r.HealthCheck()

	r.RouterWebUI()
}

func (r *EchoFramework) HealthCheck() {
	r.GET("/health_check", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "OK")
	})
}
