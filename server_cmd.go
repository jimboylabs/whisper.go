package main

import (
	"log"

	"github.com/spf13/cobra"
	"go.temporal.io/sdk/client"
)

var (
	serverCmd = &cobra.Command{
		Use:  "server",
		Long: "",
		RunE: func(cmd *cobra.Command, args []string) error {
			c, err := client.Dial(client.Options{
				HostPort: "localhost:7233",
			})
			if err != nil {
				log.Fatal("unable to create temporal.io client", err)
			}
			defer c.Close()

			app := &App{Temporal: c}
			app.StartHTTPServer()

			return nil
		},
	}
)
