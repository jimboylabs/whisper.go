package main

import (
	"os"

	"github.com/spf13/cobra"
)

var (
	// Version as provided by goreleaser
	Version = ""

	// CommitSHA as provided by goreleaser
	CommitSHA = ""

	rootCmd = &cobra.Command{
		Use:   "whisper",
		Short: "",
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(workerCmd)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
