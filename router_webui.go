package main

import (
	"embed"
	"encoding/csv"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
	"github.com/oklog/ulid/v2"
	"go.temporal.io/sdk/client"
	"whisper.go/workflow"
)

//go:embed webui/assets/*.js
var assetFiles embed.FS

func (r *EchoFramework) RouterWebUI() {
	assetsFS, err := fs.Sub(assetFiles, "webui/assets")
	if err != nil {
		log.Fatal(err)
	}

	r.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "index", "Hello")
	})

	assetsHandler := http.FileServer(http.FS(assetsFS))

	r.GET("/assets/*", echo.WrapHandler(http.StripPrefix("/assets/", assetsHandler)))

	r.POST("/upload", func(c echo.Context) error {
		file, err := c.FormFile("file")
		if err != nil {
			return err
		}

		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		id := ulid.Make()

		ext := filepath.Ext(file.Filename)

		newFilename := id.String() + ext

		dst, err := os.Create(filepath.Join("data", newFilename))
		if err != nil {
			return err
		}
		defer dst.Close()

		if _, err := io.Copy(dst, src); err != nil {
			return err
		}

		ctx := c.Request().Context()

		wo := client.StartWorkflowOptions{
			ID:        id.String(),
			TaskQueue: workflow.WhisperTaskQueue,
		}

		we, err := r.Temporal.ExecuteWorkflow(ctx, wo, workflow.TranscribeWorkflow, newFilename)
		if err != nil {
			return err
		}

		workflowID := we.GetID()
		runID := we.GetRunID()

		params := url.Values{}
		params.Add("workflow_id", workflowID)
		params.Add("run_id", runID)

		// return c.HTML(http.StatusOK, fmt.Sprintf(`<div id="message" hx-get="/transcribe?%s" hx-trigger="every 3s">%s</div>`, params.Encode(), "started"))

		return c.Render(http.StatusOK, "transcribe", map[string]interface{}{
			"queryString": params.Encode(),
		})
	})

	r.GET("/waiting-message", func(c echo.Context) error {

		var workflowID, runID string

		if c.QueryParam("workflow_id") == "" || c.QueryParam("run_id") == "" {
			return c.HTML(http.StatusNotFound, "Transcribe ID not found.")
		}

		workflowID = c.QueryParam("workflow_id")
		runID = c.QueryParam("run_id")

		ctx := c.Request().Context()
		queryType := "waiting_message"

		response, err := r.Temporal.QueryWorkflow(ctx, workflowID, runID, queryType)
		if err != nil {
			return c.HTML(http.StatusInternalServerError, "Failed to query transcribe id")
		}

		var result string

		err = response.Get(&result)
		if err != nil {
			return c.HTML(http.StatusInternalServerError, "Failed to query transcribe id")
		}

		if result == "done" {
			return c.HTML(286, result)
		}

		return c.HTML(http.StatusOK, result)
	})

	r.GET("/transcribe", func(c echo.Context) error {
		var workflowID, runID string

		if c.QueryParam("workflow_id") == "" || c.QueryParam("run_id") == "" {
			return c.HTML(http.StatusNotFound, "Transcribe ID not found.")
		}

		workflowID = c.QueryParam("workflow_id")
		runID = c.QueryParam("run_id")

		ctx := c.Request().Context()
		queryType := "current_state"

		response, err := r.Temporal.QueryWorkflow(ctx, workflowID, runID, queryType)
		if err != nil {
			return c.HTML(http.StatusInternalServerError, "Failed to query transcribe id")
		}

		var result string

		err = response.Get(&result)
		if err != nil {
			return c.HTML(http.StatusInternalServerError, "Failed to query transcribe id")
		}

		if result == "done" {
			// open csv file
			wd, err := os.Getwd()
			if err != nil {
				log.Error("get wed", err)
				return c.HTML(http.StatusInternalServerError, "Something went wrong!")
			}

			filename := fmt.Sprintf("%s.wav.csv", workflowID)

			file, err := os.Open(filepath.Join(wd, "data", filename))
			if err != nil {
				log.Error("os.Open", err)
				return c.HTML(http.StatusInternalServerError, "Something went wrong!")
			}
			defer file.Close()

			reader := csv.NewReader(file)
			records, err := reader.ReadAll()
			if err != nil {
				log.Error("read file", err)
				return c.HTML(http.StatusInternalServerError, "Something went wrong!")
			}

			var output string

			for _, record := range records {
				output += strings.Join(record, " ") + "<br>"
			}

			return c.HTML(286, output)
		}

		return c.HTML(http.StatusOK, result)
	})
}
