package ffmpegexec

import (
	"context"
	"os"
	"path/filepath"
	"strings"

	"github.com/charmbracelet/log"
)

type ConvertToWAVParams struct {
	Filename string
}

func ConvertToWAV(ctx context.Context, params ConvertToWAVParams) (string, error) {
	dstFilename := strings.Replace(params.Filename, "mp3", "wav", 1)

	args := []string{"-i", params.Filename, "-ar", "16000", "-ac", "1", "-c:a", "pcm_s16le", dstFilename}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return dstFilename, err
	}

	stdLog := log.StandardLog(log.StandardLogOptions{
		ForceLevel: log.ErrorLevel,
	})

	execParams := ffmpegExecParams{
		ffmpegPath: "/usr/bin/ffmpeg",
		args:       args,
		workDir:    filepath.Join(homeDir, "/clones/whisper.go/data"),
		stdErr:     stdLog.Writer(),
		stdOut:     stdLog.Writer(),
	}

	err = ffmpegExec(ctx, execParams)
	if err != nil {
		return dstFilename, err
	}

	return dstFilename, err
}
