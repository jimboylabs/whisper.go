package workflow

import (
	"context"

	"whisper.go/ffmpegexec"
	"whisper.go/whisperexec"
)

type WhisperActivity struct {
}

type TranscribeRequest struct {
	Filename string
}

type ConvertToWAVRequest struct {
	Filename string
}

func (a *WhisperActivity) ConvertToWAV(ctx context.Context, request ConvertToWAVRequest) (string, error) {
	result, err := ffmpegexec.ConvertToWAV(ctx, ffmpegexec.ConvertToWAVParams{
		Filename: request.Filename,
	})
	if err != nil {
		return result, err
	}

	return result, nil
}

func (a *WhisperActivity) Transcribe(ctx context.Context, request TranscribeRequest) (string, error) {
	err := whisperexec.Make(ctx, whisperexec.MakeParams{
		Filename: request.Filename,
	})
	if err != nil {
		return "FAILED", err
	}

	return "OK", nil
}
