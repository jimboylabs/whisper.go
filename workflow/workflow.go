package workflow

import (
	"path/filepath"
	"time"

	"go.temporal.io/sdk/workflow"
)

func TranscribeWorkflow(ctx workflow.Context, filename string) (string, error) {
	var (
		a              *WhisperActivity
		currentState   = "started"
		waitingMessage = "please wait..."
	)

	err := workflow.SetQueryHandler(ctx, "current_state", func() (string, error) {
		return currentState, nil
	})
	if err != nil {
		currentState = "failed to register query handler"
		return "FAILED", err
	}

	err = workflow.SetQueryHandler(ctx, "waiting_message", func() (string, error) {
		return waitingMessage, nil
	})
	if err != nil {
		waitingMessage = "failed to register query handler"
		return "FAILED", err
	}

	if filepath.Ext(filename) == ".mp3" {
		activityOptions := workflow.ActivityOptions{
			StartToCloseTimeout: time.Minute * 5,
		}
		activityCtx := workflow.WithActivityOptions(ctx, activityOptions)

		req := ConvertToWAVRequest{
			Filename: filename,
		}

		var convertedFilename string
		err := workflow.ExecuteActivity(activityCtx, a.ConvertToWAV, req).Get(activityCtx, &convertedFilename)
		if err != nil {
			return "FAILED", nil
		}
		filename = convertedFilename
	}

	activityOptions := workflow.ActivityOptions{
		StartToCloseTimeout: time.Minute * 30,
	}
	activityCtx := workflow.WithActivityOptions(ctx, activityOptions)

	childCtx, cancelHandler := workflow.WithCancel(activityCtx)
	selector := workflow.NewSelector(activityCtx)

	req := TranscribeRequest{
		Filename: filename,
	}

	var result string
	f := workflow.ExecuteActivity(activityCtx, a.Transcribe, req)

	selector.AddFuture(f, func(f workflow.Future) {
		err := f.Get(activityCtx, &result)
		if err != nil {
			currentState = "failed to execute transcribe activity"
		}

		waitingMessage = ""

		currentState = "done"

		// cancel timerFuture
		cancelHandler()
	})

	// use timer to update waiting message
	timerFuture := workflow.NewTimer(childCtx, 5*time.Minute)
	selector.AddFuture(timerFuture, func(f workflow.Future) {
		if currentState != "done" {
			waitingMessage = "Might take longer than usual..."
		}
	})

	// wait the timer or the transcibe processing to finish
	selector.Select(activityCtx)

	if currentState != "done" {
		selector.Select(activityCtx)
	}

	workflow.GetLogger(activityCtx).Info("Workflow completed.")

	return result, nil
}
