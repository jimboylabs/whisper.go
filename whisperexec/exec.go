package whisperexec

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"syscall"
	"time"
)

type whisperExecParams struct {
	whisperPath string
	args        []string
	stdErr      io.Writer
	stdOut      io.Writer
	workDir     string
}

func whisperExec(ctx context.Context, run whisperExecParams) error {
	exited := false
	defer func() {
		exited = true
	}()

	cmd := exec.Command(run.whisperPath, run.args...)
	cmd.Dir = run.workDir
	cmd.SysProcAttr = osSpecificSysProcAttr()

	cmd.Stdout = io.MultiWriter(run.stdOut)
	cmd.Stderr = io.MultiWriter(run.stdErr)

	if ctx.Err() != nil {
		return ctx.Err()
	}

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("whisper exec command error: %w", err)
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// cmd.Start ensures that cmd.Process is non nil
	go func() {
		// Wait for context to be cancelled
		<-ctx.Done()

		if exited {
			return
		}

		if err := syscall.Kill(-cmd.Process.Pid, syscall.SIGINT); err != nil {
			if errors.Is(os.ErrProcessDone, err) {
				return
			}

			_ = syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
			_ = cmd.Process.Kill()
		}

		deadline := time.Now().Add(30 * time.Second)
		for time.Now().Before(deadline) {
			<-time.After(200 * time.Millisecond)
			if exited {
				return
			}
		}

		_ = syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
		_ = cmd.Process.Kill()
	}()

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf("whisper error: %w", err)
	}

	return nil
}
