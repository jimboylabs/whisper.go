package whisperexec

import (
	"context"
	"os"
	"path/filepath"

	"github.com/charmbracelet/log"
)

type MakeParams struct {
	Filename string
}

func Make(ctx context.Context, params MakeParams) error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	args := []string{"-f", filepath.Join(wd, "data", params.Filename), "--output-csv"}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	stdlog := log.StandardLog(log.StandardLogOptions{
		ForceLevel: log.ErrorLevel,
	})

	execParams := whisperExecParams{
		whisperPath: "./main",
		args:        args,
		workDir:     filepath.Join(homeDir, "/clones/whisper.cpp"),
		stdErr:      stdlog.Writer(),
		stdOut:      stdlog.Writer(),
	}

	err = whisperExec(ctx, execParams)
	if err != nil {
		return err
	}

	return nil
}
