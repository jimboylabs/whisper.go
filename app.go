package main

import (
	"fmt"

	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
	"go.temporal.io/sdk/client"
	"whisper.go/webui/templates"
)

// put app dependencies like database here
type App struct {
	Temporal client.Client
}

const DefaultPort = 3000

func (a *App) StartHTTPServer() {
	e := echo.New()

	t, err := templates.NewTemplates()
	if err != nil {
		log.Fatal(err)
	}

	// register templates renderer
	tr := templates.NewIndexView(t)
	e.Renderer = tr

	SetupEchoRouter(e, a)

	listenerPort := fmt.Sprintf(":%d", DefaultPort)
	e.Logger.Fatal(e.Start(listenerPort))
}
