package main

import (
	"github.com/spf13/cobra"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
	"whisper.go/workflow"
)

var (
	workerCmd = &cobra.Command{
		Use:  "worker",
		Long: "",
		RunE: func(cmd *cobra.Command, args []string) error {
			client, err := client.Dial(client.Options{
				HostPort: "localhost:7233",
			})
			if err != nil {
				return nil
			}
			defer client.Close()

			// you can put dependencies here
			// such as database if needed
			a := workflow.WhisperActivity{}

			w := worker.New(client, workflow.WhisperTaskQueue, worker.Options{
				MaxConcurrentActivityExecutionSize: 2,
			})
			w.RegisterActivity(a.ConvertToWAV)
			w.RegisterActivity(a.Transcribe)
			w.RegisterWorkflow(workflow.TranscribeWorkflow)

			err = w.Run(worker.InterruptCh())
			if err != nil {
				return err
			}

			return nil
		},
	}
)
